---
title:        "The Fellow Gamer Podcast Trailer (Season 1)"
subtitle:     "Optional subtitle here"
date:         2019-11-09 16:29:02 +0800
keywords:     
- tfgp
- podcast-trailer
duration:     52
audio-url:    "/assets/audio/season-1/tfgp-trailer.m4a"
episode-type: trailer # or "full"
explicit:     "no"
block:        "no" # no means it is published
layout: podcast
excerpt_separator: <!--more-->

---
Excerpt here.
<!--more-->

More details of your episode here.
